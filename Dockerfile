FROM ubuntu
RUN apt update && apt install -y nodejs
EXPOSE 80
COPY hello.js /usr/local/sbin
ENTRYPOINT hello.js
